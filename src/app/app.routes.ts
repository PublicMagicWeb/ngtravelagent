import {
    Routes
} from '@angular/router';
import {
    RouterContainerMagicComponent
} from "@magic-xpa/angular";
export const routes: Routes = [
    {   path: 'Packages',
        component: RouterContainerMagicComponent,
    },
    {
        path: 'EditPackages/:pRecord/:pLine',
        component: RouterContainerMagicComponent,
    },
    // {   path: 'FlightDetails/:pRecord/:pLine',
    //     component: RouterContainerMagicComponent,
    // },
    {
        path:'',
        redirectTo:'Packages',
        pathMatch: 'full'
    },
];