import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-carInformation',
    providers: [...magicProviders],
    styleUrls: ['./carInformation.component.css'],
    templateUrl: './carInformation.component.html'
}) export class carInformation extends TaskBaseMagicComponent {}