import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-EditTravelRequestEntries',
    providers: [...magicProviders],
    styleUrls: ['./EditTravelRequestEntries.component.css'],
    templateUrl: './EditTravelRequestEntries.component.html'
}) export class EditTravelRequestEntries extends TaskBaseMagicComponent {
    displayedColumns = ['Column1048588',
        'Column1048589',
    ];
}