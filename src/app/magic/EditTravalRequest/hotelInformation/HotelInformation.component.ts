import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
import {
    MagicModalInterface
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-HotelInformation',
    providers: [...magicProviders],
    styleUrls: ['./HotelInformation.component.css'],
    templateUrl: './HotelInformation.component.html'
}) export class HotelInformation extends TaskBaseMagicComponent implements MagicModalInterface {
    private static readonly formName: string = "HotelInformation";
    private static readonly showTitleBar: boolean = true;
    private static readonly x: number = 0;
    private static readonly y: number = 0;
    private static readonly width: number = 300;
    private static readonly height: number = 300;
    private static readonly isCenteredToWindow: boolean = true;
    private static readonly shouldCloseOnBackgroundClick = true;
    X() {
        return HotelInformation.x;
    }
    Y() {
        return HotelInformation.y;
    }
    Width() {
        return HotelInformation.width;
    }
    Height() {
        return HotelInformation.height;
    }
    IsCenteredToWindow() {
        return HotelInformation.isCenteredToWindow;
    }
    FormName() {
        return HotelInformation.formName;
    }
    ShowTitleBar() {
        return HotelInformation.showTitleBar;
    }
    ShouldCloseOnBackgroundClick() {
        return HotelInformation.shouldCloseOnBackgroundClick;
    }
}