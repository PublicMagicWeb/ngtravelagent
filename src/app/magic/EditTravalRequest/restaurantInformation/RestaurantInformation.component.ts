import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-RestaurantInformation',
    providers: [...magicProviders],
    styleUrls: ['./RestaurantInformation.component.css'],
    templateUrl: './RestaurantInformation.component.html'
}) export class RestaurantInformation extends TaskBaseMagicComponent {}