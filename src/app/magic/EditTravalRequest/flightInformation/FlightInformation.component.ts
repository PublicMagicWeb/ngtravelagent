import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-FlightInformation',
    providers: [...magicProviders],
    styleUrls: ['./FlightInformation.component.css'],
    templateUrl: './FlightInformation.component.html'
}) export class FlightInformation extends TaskBaseMagicComponent {}