import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-showlargeimage',
    providers: [...magicProviders],
    styleUrls: ['./showlargeimage.component.css'],
    templateUrl: './showlargeimage.component.html'
}) export class showlargeimage extends TaskBaseMagicComponent {}