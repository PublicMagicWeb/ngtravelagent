import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-SelectFile',
    providers: [...magicProviders],
    styleUrls: ['./SelectFile.component.css'],
    templateUrl: './SelectFile.component.html'
}) export class SelectFile extends TaskBaseMagicComponent {}