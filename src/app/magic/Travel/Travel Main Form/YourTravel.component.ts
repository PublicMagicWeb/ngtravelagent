import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-YourTravel',
    providers: [...magicProviders],
    styleUrls: ['./YourTravel.component.css'],
    templateUrl: './YourTravel.component.html'
}) export class YourTravel extends TaskBaseMagicComponent {}