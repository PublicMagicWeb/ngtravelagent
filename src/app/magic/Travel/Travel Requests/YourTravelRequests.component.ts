import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-YourTravelRequests',
    providers: [...magicProviders],
    styleUrls: ['./YourTravelRequests.component.css'],
    templateUrl: './YourTravelRequests.component.html'
}) export class YourTravelRequests extends TaskBaseMagicComponent {}