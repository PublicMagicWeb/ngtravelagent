import { YourTravel as Travel_Main_Form_YourTravel } from './Travel/Travel Main Form/YourTravel.component';
import { YourTravelRequests as Travel_Requests_YourTravelRequests } from './Travel/Travel Requests/YourTravelRequests.component';
import { EditTravelRequestEntries as EditTravelRequest_EditTravelRequestEntries } from './EditTravalRequest/EditTravelRequest/EditTravelRequestEntries.component';
import { FlightInformation as flightInformation_FlightInformation } from './EditTravalRequest/flightInformation/FlightInformation.component';
import { HotelInformation as hotelInformation_HotelInformation } from './EditTravalRequest/hotelInformation/HotelInformation.component';
import { RestaurantInformation as restaurantInformation_RestaurantInformation } from './EditTravalRequest/restaurantInformation/RestaurantInformation.component';
import { carInformation as carInformation_carInformation } from './EditTravalRequest/carInformation/carInformation.component';
import { showlargeimage as showlargeimage_showlargeimage } from './Utiles/showlargeimage/showlargeimage.component';
import { SelectFile as SelectFile_SelectFile } from './Utiles/SelectFile/SelectFile.component';

export const title = "";

export const magicGenCmpsHash = {               Travel_Main_Form_YourTravel:Travel_Main_Form_YourTravel,
              Travel_Requests_YourTravelRequests:Travel_Requests_YourTravelRequests,
              EditTravelRequest_EditTravelRequestEntries:EditTravelRequest_EditTravelRequestEntries,
              flightInformation_FlightInformation:flightInformation_FlightInformation,
              hotelInformation_HotelInformation:hotelInformation_HotelInformation,
              restaurantInformation_RestaurantInformation:restaurantInformation_RestaurantInformation,
              carInformation_carInformation:carInformation_carInformation,
              showlargeimage_showlargeimage:showlargeimage_showlargeimage,
              SelectFile_SelectFile:SelectFile_SelectFile,
       
};

export const magicGenComponents = [ Travel_Main_Form_YourTravel,
Travel_Requests_YourTravelRequests,
EditTravelRequest_EditTravelRequestEntries,
flightInformation_FlightInformation,
hotelInformation_HotelInformation,
restaurantInformation_RestaurantInformation,
carInformation_carInformation,
showlargeimage_showlargeimage,
SelectFile_SelectFile 
];
